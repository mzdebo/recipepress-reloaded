<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wzymedia.com
 * @since             1.0.0
 * @package           Recipepress
 *
 * @wordpress-plugin
 * Plugin Name:       Recipepress Reloaded
 * Plugin URI:        https://wzymedia.com
 * Description:       The swiss army knife for your food blog. A tool to add nicely formatted recipes that are SEO friendly to your blog and to manage your recipe collection.
 * Version:           1.6.1
 * Author:            wzy Media
 * Author URI:        https://wzymedia.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       recipepress-reloaded
 * Domain Path:       /languages
 */

namespace Recipepress;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Define Constants
 */
define( 'DS', DIRECTORY_SEPARATOR );
define( __NAMESPACE__ . '\NS', __NAMESPACE__ . '\\' );
define( NS . 'PLUGIN_NAME', 'recipepress-reloaded' );
define( NS . 'PLUGIN_VERSION', '1.6.1' );
define( NS . 'PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( NS . 'PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( NS . 'ADMIN_DIR', plugin_dir_path( __FILE__ ) . 'inc/admin/' );
define( NS . 'ADMIN_ASSET_DIR', plugin_dir_path( __FILE__ ) . 'inc/admin/assets/' );
define( NS . 'ADMIN_ASSET_URL', plugin_dir_url( __FILE__ ) . 'inc/admin/assets/' );
define( NS . 'PUB_ASSET_DIR', plugin_dir_path( __FILE__ ) . 'inc/frontend/assets/' );
define( NS . 'PUB_ASSET_URL', plugin_dir_url( __FILE__ ) . 'inc/frontend/assets/' );
define( NS . 'EXT_DIR', plugin_dir_path( __FILE__ ) . 'inc/extensions/' );
define( NS . 'EXT_URL', plugin_dir_url( __FILE__ ) . 'inc/extensions/' );
define( NS . 'LIB_DIR', plugin_dir_path( __FILE__ ) . 'inc/libraries/' );
define( NS . 'PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( NS . 'PLUGIN_TEXT_DOMAIN', 'recipepress-reloaded' );

/**
 * Autoload Classes
 */
require_once PLUGIN_DIR . 'inc/libraries/autoloader.php';

/**
 * Register Activation and Deactivation Hooks
 * This action is documented in inc/core/class-activator.php
 */
register_activation_hook( __FILE__, array( NS . 'Inc\Core\Activator', 'activate' ) );

/**
 * The code that runs during plugin deactivation.
 * This action is documented inc/core/class-deactivator.php
 */
register_deactivation_hook( __FILE__, array( NS . 'Inc\Core\Deactivator', 'deactivate' ) );


/**
 * Plugin Singleton Container
 *
 * Maintains a single copy of the plugin Recipepress object.
 *
 * @since    1.0.0
 */
class Recipepress {

	/**
	 * The instance of the plugin.
	 *
	 * @since    1.0.0
	 * @var      Recipepress $init Instance of the plugin.
	 */
	private static $init;
	/**
	 * Loads the plugin
	 *
	 * @access    public
	 */
	public static function init() {

		if ( null === self::$init ) {
			self::$init = new Inc\Core\Init();
			self::$init->run();
		}

		return self::$init;
	}

}

/**
 * Begins execution of the plugin
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * Also returns copy of the app object so 3rd party developers
 * can interact with the plugin's hooks contained within.
 **/
function recipepress_init() {
	return Recipepress::init();
}

$min_php = '5.6.0';

// Check the minimum required PHP version and run the plugin.
if ( version_compare( PHP_VERSION, $min_php, '>=' ) ) {
	recipepress_init();
}
