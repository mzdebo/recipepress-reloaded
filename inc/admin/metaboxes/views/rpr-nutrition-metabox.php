<?php
/**
 * The nutritional informational metabox view of the plugin.
 *
 * @since 1.0.0
 *
 * @var \WP_Post $recipe
 * @var \Recipepress\Inc\Admin\Metaboxes\Nutrition $this
 *
 * @package    recipepress-reloaded
 * @subpackage recipepress-reloaded/admin/views
 */

$this->create_nonce();

$calorific_value = get_post_meta( $recipe->ID, 'rpr_recipe_calorific_value', true );
$protein         = get_post_meta( $recipe->ID, 'rpr_recipe_protein', true );
$fat             = get_post_meta( $recipe->ID, 'rpr_recipe_fat', true );
$carbohydrate    = get_post_meta( $recipe->ID, 'rpr_recipe_carbohydrate', true );
$per             = get_post_meta( $recipe->ID, 'rpr_recipe_nutrition_per', true );
?>

<div class="rpr_nutrition_metabox">
	<div class="recipe_details_row">
		<div class="rpr_nutrition_row">
			<label for="rpr_recipe_calorific_value"><?php esc_attr_e( 'Calories', 'recipepress-reloaded' ); ?>:</label>
			<input type="number" min="0" step="any" name="rpr_recipe_calorific_value" id="rpr_recipe_calorific_value"
				value="<?php echo esc_attr( $calorific_value ); ?>" placeholder="0"/>
			<span class="recipe-general-form-notes">kcal</span>
		</div>
		<div class="rpr_nutrition_row">
			<label for="rpr_recipe_calorific_value_kj"><?php esc_attr_e( 'Joules', 'recipepress-reloaded' ); ?>:</label>
			<input type="number" min="0" step="any" name="rpr_recipe_calorific_value_kj" id="rpr_recipe_calorific_value_kj"
				value="<?php echo $calorific_value ? esc_attr( round( 4.18 * (int) $calorific_value ) ) : ''; ?>"
				placeholder="0"/>
			<span class="recipe-general-form-notes">kJ</span>
		</div>
		<div class="rpr_nutrition_row">
			<label for="rpr_recipe_nutrition_per"><?php esc_attr_e( 'Per', 'recipepress-reloaded' ); ?>:</label>
			<select name="rpr_recipe_nutrition_per" id="rpr_recipe_nutrition_per">
				<option value="per_serving" <?php selected( $per, 'per_serving' ); ?>>
					<?php esc_attr_e( 'serving', 'recipepress-reloaded' ); ?>
				</option>
				<option value="per_recipe" <?php selected( $per, 'per_recipe' ); ?>>
					<?php esc_attr_e( 'recipe', 'recipepress-reloaded' ); ?>
				</option>
				<option value="per_portion" <?php selected( $per, 'per_portion' ); ?>>
					<?php esc_attr_e( 'portion', 'recipepress-reloaded' ); ?>
				</option>
				<option value="per_100g" <?php selected( $per, 'per_100g' ); ?>>
					<?php esc_attr_e( '100g', 'recipepress-reloaded' ); ?>
				</option>
			</select>
		</div>
	</div>

	<div class="recipe_details_row">
		<div class="rpr_nutrition_row rpr_protein">
			<label for="rpr_recipe_protein"><?php esc_attr_e( 'Protein', 'recipepress-reloaded' ); ?>:</label>
			<input type="number" min="0" step="any" name="rpr_recipe_protein" id="rpr_recipe_protein"
				value="<?php echo esc_attr( $protein ); ?>" placeholder="0"/>
			<span class="recipe-general-form-notes"><?php esc_attr_e( 'grams', 'recipepress-reloaded' ); ?></span>
		</div>
		<div class="rpr_nutrition_row rpr_fat">
			<label for="rpr_recipe_fat"><?php esc_attr_e( 'Fat', 'recipepress-reloaded' ); ?>:</label>
			<input type="number" min="0" step="any" name="rpr_recipe_fat" id="rpr_recipe_fat"
				value="<?php echo esc_attr( $fat ); ?>" placeholder="0"/>
			<span class="recipe-general-form-notes"><?php esc_attr_e( 'grams', 'recipepress-reloaded' ); ?></span>
		</div>
		<div class="rpr_nutrition_row rpr_carbohydrate">
			<label for="rpr_recipe_carbohydrate"><?php esc_attr_e( 'Carbs', 'recipepress-reloaded' ); ?>:</label>
			<input type="number" min="0" step="any" name="rpr_recipe_carbohydrate" id="rpr_recipe_carbohydrate"
				value="<?php echo esc_attr( $carbohydrate ); ?>" placeholder="0"/>
			<span class="recipe-general-form-notes"><?php esc_attr_e( 'grams', 'recipepress-reloaded' ); ?></span>
		</div>
	</div>

</div>
