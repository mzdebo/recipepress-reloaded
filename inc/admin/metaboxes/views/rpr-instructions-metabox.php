<?php
/**
 * The instructions metabox view of the plugin.
 *
 * @link       http://tech.cbjck.de/wp/rpr
 * @since      0.8.0
 *
 * @var object $recipe The recipe post object.
 *
 * @package    recipepress-reloaded
 * @subpackage recipepress-reloaded/admin/views
 */

$this->create_nonce();
?>

<table width="100%" cellspacing="5" class="rpr-metabox-table instructions" id="recipe-instructions">
	<thead>
	<tr>
		<th class="rpr-ins-sort">
			<div class="dashicons dashicons-sort"></div>
		</th>
		<th class="rpr-ins-instruction"><?php esc_html_e( 'Description', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ins-image"><?php esc_html_e( 'Image', 'recipepress-reloaded' ); ?></th>
		<th class="rpr-ing-del"></th>
	</tr>
	</thead>
	<tbody>
	<!-- hidden row to copy heading lines from -->
	<tr class="instruction-group-stub rpr-hidden">
		<td class="rpr-ins-sort">
			<div class="sort-handle dashicons dashicons-sort"></div>
			<input type="hidden" name="rpr_recipe_instructions[0][sort]" class="instructions_sort" id="instructions_sort_0" />
		</td>
		<td colspan="2" class="rpr-ins-group">
			<label for="instructions_grouptitle_0" class="screen-reader-text">
				<?php esc_html_e( 'Instruction Group', 'recipepress-reloaded' ); ?>
			</label>
			<input type="text" class="instructions-group-label instructions_grouptitle"
				   name="rpr_recipe_instructions[0][grouptitle]"
				   id="instructions_grouptitle_0"
				   placeholder="Instruction group title" />
		</td>
		<td class="rpr-ins-del">
			<button class="rpr-ins-add-row button dashicons dashicons-plus"
					data-type="rpr_instructions"
					title="<?php esc_attr_e( 'Add row', 'recipepress-reloaded' ); ?>"></button>
			<button class="rpr-ins-remove-row button dashicons dashicons-no"
					data-type="rpr_instructions"
					title="<?php esc_attr_e( 'Remove row', 'recipepress-reloaded' ); ?>"></button>
		</td>
	</tr>

	<!-- Existing ingredient rows -->
	<?php
	$instructions = get_post_meta( $recipe->ID, 'rpr_recipe_instructions', true );
	$i            = 1;

	if ( is_array( $instructions ) ) {
		foreach ( $instructions as $ins ) {
			$has_image = '';

			// Check if we have an instruction group, or an instruction line.
			if ( isset( $ins['grouptitle'] ) ) {
				// we have an instruction group title line
				// Add a group heading line.
				if ( '' !== $ins['grouptitle'] ) {
					?>
					<tr class="instruction-group">
						<td class="rpr-ins-sort">
							<div class="sort-handle dashicons dashicons-sort"></div>
							<input type="hidden" name="rpr_recipe_instructions[<?php echo $i; ?>][sort]" class="instructions_sort" id="instructions_sort_<?php echo $i; ?>" value="<?php echo $ins['sort']; ?>" />
						</td>
						<td colspan="2" class="rpr-ins-group">
							<label for="instructions_grouptitle_<?php echo $i; ?>" class="screen-reader-text">
								<?php esc_html_e( 'Instruction Group', 'recipepress-reloaded' ); ?>
							</label>
							<input type="text" class="instructions-group-label instructions_grouptitle"
								   name="rpr_recipe_instructions[<?php echo $i; ?>][grouptitle]"
								   id="instructions_grouptitle_<?php echo $i; ?>"
								   value="<?php echo $ins['grouptitle']; ?>" />
						</td>
						<td class="rpr-ins-del">
							<button class="rpr-ins-add-row button dashicons dashicons-plus"
									data-type="rpr_instructions"
									title="<?php esc_attr_e( 'Add group', 'recipepress-reloaded' ); ?>"></button>
							<button class="rpr-ins-remove-row button dashicons dashicons-no"
									data-type="rpr_instructions"
									title="<?php esc_attr_e( 'Remove group', 'recipepress-reloaded' ); ?>"></button>
						</td>
					</tr>
					<?php
				}
			} else {
				// We have an instruction line.
				$image = '';

				if ( ! empty( $ins['image'] ) && wp_get_attachment_image_src( $ins['image'], 'recipe-thumbnail' ) ) {
					$image = wp_get_attachment_image_src( $ins['image'], 'recipe-thumbnail' )[0];
				}
				?>
				<tr class="rpr-ins-row">
					<td class="rpr-ins-sort">
						<div class="sort-handle dashicons dashicons-sort"></div>
						<input type="hidden"
							   name="rpr_recipe_instructions[<?php echo $i; ?>][sort]"
							   class="instructions_sort"
							   id="instructions_sort_<?php echo $i; ?>"
							   value="<?php echo $ins['sort']; ?>" />
					</td>
					<td class="rpr-ins-instruction">
						<label class="screen-reader-text" for="instruction_description_<?php echo $i; ?>">
							<?php printf( '%s %d', __( 'Step', 'recipepress-reloaded' ), $i ); ?>
						</label>
						<textarea name="rpr_recipe_instructions[<?php echo $i; ?>][description]"
								  rows="4" id="instruction_description_<?php echo $i; ?>"><?php echo $ins['description']; ?></textarea>
					</td>
					<td class="rpr-ins-image">
						<div class="rpr-ins-image-wrapper rpr-ins-image-set"
							 id="rpr_recipe_instructions_image_set_<?php echo $i; ?>"
							 data-recipe-id="<?php echo $recipe->ID; ?>"
						>
							<span title="<?php esc_attr_e( 'Remove instruction image' ) ?>"
								  id="rpr_recipe_instructions_image_del_<?php echo $i; ?>"
								  style="<?php printf( 'display: %s', ( $image ? 'inline' : 'none' ) ); ?>"
								  class="rpr-ins-image-del dashicons dashicons-trash"></span>
							<span title="<?php esc_attr_e( 'Add instruction image', 'recipepress-reloaded' ) ?>"
								  id="rpr_recipe_instructions_image_set_<?php echo $i; ?>"
								  style="<?php printf( 'display: %s', ( $image ? 'none' : 'inline' ) ); ?>"
								  class="rpr-ins-image-set dashicons dashicons-format-image" rel="<?php echo $recipe->ID; ?>"></span>
								<img class="rpr_recipe_instructions_thumbnail"
									 id="rpr_recipe_instructions_thumbnail_<?php echo $i; ?>"
									 src="<?php echo $image; ?>"
									 style="<?php printf( 'display: %s', ( $image ? 'block' : 'none' ) ); ?>"
									 alt="<?php printf( '%s %d', __( 'Process photograph for step', 'recipepress-reloaded' ), $i ); ?>"/>
							<input name="rpr_recipe_instructions[<?php echo $i; ?>][image]" class="rpr_recipe_instructions_image" type="hidden" value="<?php echo $ins['image']; ?>" />
						</div>
					</td>
					<td class="rpr-ins-del">
						<button class="rpr-ins-add-row button dashicons dashicons-plus" data-type="rpr_instructions" title="<?php esc_attr_e( 'Add instruction', 'recipepress-reloaded' ); ?>"></button>
						<button class="rpr-ins-remove-row button dashicons dashicons-no" data-type="rpr_instruction" title="<?php esc_attr_e( 'Remove instruction', 'recipepress-reloaded' ); ?>"></button>
					</td>
				</tr>
				<?php
			}
			$i++;
		}
	}
	?>
	<!-- the last row is always empty, in case you want to add some -->
	<tr class="rpr-ins-row">
		<td class="rpr-ins-sort">
			<div class="sort-handle dashicons dashicons-sort"></div>
			<input type="hidden" name="rpr_recipe_instructions[<?php echo $i; ?>][sort]" class="instructions_sort" id="instructions_sort_<?php echo $i; ?>" />
		</td>
		<td class="rpr-ins-instruction">
			<label class="screen-reader-text" for="instruction_description_<?php echo $i; ?>">
				<?php printf( '%s %d', __( 'Step', 'recipepress-reloaded' ), $i ); ?>
			</label>
			<textarea name="rpr_recipe_instructions[<?php echo $i; ?>][description]" rows="4" id="instruction_description_<?php echo $i; ?>"></textarea>
		</td>
		<td class="rpr-ins-image">
			<div class="rpr-ins-image-wrapper rpr-ins-image-set"
				 id="rpr_recipe_instructions_image_set_<?php echo $i; ?>"
				 data-recipe-id="<?php echo $recipe->ID; ?>"
			>
				<span class="dashicons dashicons-format-image"></span>
				<span title="<?php esc_attr_e( 'Remove instruction image', 'recipepress-reloaded' ); ?>"
					  id="rpr_recipe_instructions_image_del_<?php echo $i; ?>"
					  style="display:none" class="rpr-ins-image-del dashicons dashicons-trash" ></span>
				<img class="rpr_recipe_instructions_thumbnail" id="rpr_recipe_instructions_thumbnail_<?php echo $i; ?>" src="" style="display:none;"/>
				<input name="rpr_recipe_instructions[<?php echo $i; ?>][image]" class="rpr_recipe_instructions_image" type="hidden" value="" />
			</div>
		</td>
		<td class="rpr-ins-del">
			<button class="rpr-ins-add-row button dashicons dashicons-plus"
					data-type="rpr_instructions"
					title="<?php esc_attr_e( 'Add instruction', 'recipepress-reloaded' ); ?>"></button>
			<button class="rpr-ins-remove-row button dashicons dashicons-no"
					data-type="rpr_instruction"
					title="<?php esc_attr_e( 'Remove instruction', 'recipepress-reloaded' ); ?>"></button>
		</td>
	</tr>
	</tbody>

	<tfoot>
	<tr>
		<td colspan="7" style="padding: 3em 0 0 0">
			<button id="rpr-ins-add-row-ins" class="rpr-ins-add-row button" data-type="rpr_instruction" href="#">
				<span class="dashicons dashicons-plus"></span>
				<?php esc_html_e( 'Add an instruction', 'recipepress-reloaded' ); ?>
			</button>
			<button id="rpr-ins-add-row-grp" class="rpr-ins-add-row button" data-type="heading" href="#">
				<span class="dashicons dashicons-plus"></span>
				<?php esc_html_e( 'Add a group', 'recipepress-reloaded' ); ?>
			</button>
		</td>
	</tr>
	</tfoot>
</table>
