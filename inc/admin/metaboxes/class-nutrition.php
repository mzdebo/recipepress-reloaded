<?php

namespace Recipepress\Inc\Admin\Metaboxes;

use Recipepress\Inc\Common\Abstracts\Metadata;

/**
 * Saving the recipe nutrition meta information.
 *
 * @package    Recipepress
 * @subpackage Recipepress/inc/admin/metaboxes
 * @author     Kemory Grubb <kemory@wzymedia.com>
 */
class Nutrition extends Metadata {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since   1.0.0
	 *
	 * @param   string $plugin_name The ID of this plugin.
	 * @param   string $version     The current version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		parent::__construct( $plugin_name, $version, 'rpr_nutrition_metabox', __DIR__, true, true );
	}

	/**
	 * Add a metabox to the WP post edit screen
	 *
	 * @since 1.0.0
	 *
	 * @uses  add_meta_box
	 *
	 * @return bool
	 */
	public function add_metabox() {

		add_meta_box(
			$this->metabox_id,
			__( 'Nutritional information', 'recipepress-reloaded' ),
			array( $this, 'render_metabox' ),
			'rpr_recipe',
			'side',
			'high'
		);

		return true;
	}

	/**
	 * Should we display this metabox.
	 *
	 * @since 1.0.0
	 *
	 * @return bool
	 */
	protected function display_metabox() {

		return true;
	}

	/**
	 * Check the presence of, sanitizes then saves the metabox's data.
	 *
	 * @since 1.0.0
	 *
	 * @uses  $this->check_nonce()
	 * @uses  update_post_meta()
	 * @uses  wp_verify_nonce()
	 * @uses  sanitize_text_field()
	 *
	 * @param int      $recipe_id The post ID of the recipe post.
	 * @param array    $data      The data passed from the post custom metabox.
	 * @param \WP_Post $recipe    The recipe object this data is being saved to.
	 *
	 * @return bool|int
	 */
	public function save_metabox_metadata( $recipe_id, $data, $recipe ) {

		if ( ! $this->check_nonce( $data ) ) {
			return false;
		}

		$fields = array(
			'rpr_recipe_calorific_value',
			'rpr_recipe_protein',
			'rpr_recipe_fat',
			'rpr_recipe_carbohydrate',
			'rpr_recipe_nutrition_per',
		);

		foreach ( $fields as $key ) {
			if ( isset( $data[ $key ] ) ) {
				$old = get_post_meta( $recipe_id, $key, true );
				$new = $data[ $key ];

				if ( $new !== $old ) {
					update_post_meta( $recipe_id, $key, $new );
				} elseif ( '' === $new && $old ) {
					delete_post_meta( $recipe_id, $key, $old );
				}
			}
		}

		return $recipe_id;
	}

}
