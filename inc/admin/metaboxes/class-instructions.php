<?php

namespace Recipepress\Inc\Admin\Metaboxes;

use Recipepress\Inc\Core\Options;
use Recipepress\Inc\Common\Abstracts\MetaData;
use Recipepress\Inc\Frontend\Template;

/**
 * Saving the instructions meta information.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Recipepress
 * @subpackage Recipepress/inc/common/metadata/book/
 * @author     Kemory Grubb <kemory@wzymedia.com>
 */
class Instructions extends Metadata {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since   1.0.0
	 * @var \Recipepress\Inc\Frontend\Template $template
	 */
	 private $template;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since   1.0.0
	 *
	 * @param   string $plugin_name The ID of this plugin.
	 * @param   string $version The current version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		parent::__construct( $plugin_name, $version, 'rpr_instructions_metabox', __DIR__, true, true );

		$this->template = new Template( $plugin_name, $version );
	}

	/**
	 * Add a metabox for the recipe instruction.
	 *
	 * If the option has been disabled on the plugin setting page, return early with a false
	 * and don't do anything.
	 *
	 * @since 1.0.0
	 *
	 * @uses  add_meta_box
	 * @return bool
	 */
	public function add_metabox() {

		add_meta_box(
			$this->metabox_id,
			__( 'Instructions', 'recipepress-reloaded' ),
			array( $this, 'render_metabox' ),
			'rpr_recipe',
			'normal',
			'high'
		);

		return true;
	}

	/**
	 * Adds recipe instructions to REST response
	 *
	 * @since 1.0.0
	 *
	 * @uses  \register_rest_field()
	 *
	 * @return void
	 */
	public function register_rest_fields() {
		register_rest_field(
			'rpr_recipe',
			'rpr_recipe_instructions',
			array(
				'get_callback' => array( $this, 'get_data' ),
				'update_callback' => array( $this, 'update_data' ),
				'schema' => array(
					'description' => __( 'The recipe instructions' ),
					'type'        => 'array',
				),
			)
		);
	}

	/**
	 * Get the recipe instructions
	 *
	 * @param array $recipe The WP_POST data
	 *
	 * @return array
	 */
	public function get_data( $recipe ) {
		$metadata = $this->template->get_the_recipe_meta( $recipe['id'] );
		return isset( $metadata['rpr_recipe_instructions'] ) ? $metadata['rpr_recipe_instructions'] : array();
	}

	/**
	 * Update the recipe instructions
	 *
	 * @param array $recipe The WP_POST data
	 *
	 * @return bool
	 */
	public function update_data( $recipe ) {
		return false;
	}

	/**
	 * Should we display this metabox.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	protected function display_metabox() {
		return true;
	}

	/**
	 * Check the presence of, sanitizes then saves book's ISBN.
	 *
	 * @since 1.0.0
	 *
	 * @uses  update_post_meta()
	 * @uses  wp_verify_nonce()
	 * @uses  sanitize_text_field()
	 *
	 * @param int      $recipe_id The post ID of the recipe post.
	 * @param array    $data      The data passed from the post custom metabox.
	 * @param \WP_Post $recipe    The review object this data is being saved to.
	 *
	 * @return bool|int
	 */
	public function save_metabox_metadata( $recipe_id, $data, $recipe ) {

		if ( ! $this->check_nonce( $data ) ) {
			return false;
		}

		// A new array to contain all non-empty line from the form.
		$instructions = isset( $data['rpr_recipe_instructions'] ) ? $data['rpr_recipe_instructions'] : array();
		$non_empty    = array();

		foreach ( (array) $instructions as $ins ) {
			// Check if we have an instruction group, or an instruction line.
			if ( isset( $ins['grouptitle'] ) ) {
				// We have an ingredient group title line,
				// We do nothing and will save this line as is to the recipe metadata.
				if ( '' !== $ins['grouptitle'] ) {
					$non_empty[] = $ins;
				}
			} else {
				// We have a single ingredient line.
				if ( '' !== $ins['description'] || '' !== $ins['image'] ) {
					$non_empty[] = array_map( 'sanitize_text_field', $ins );
				}
			}
		}

		// Save the new metadata array.
		update_post_meta( $recipe_id, 'rpr_recipe_instructions', $non_empty );

		return $recipe_id;
	}

}
