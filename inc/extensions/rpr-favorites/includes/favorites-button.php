<?php
/**
 * The HTML markup of our favorite button
 *
 * @package Recipepress
 */

?>

<div id="rpr-favorites-button"></div>

<script id="favorites-button" type="text/mvl">
	<span class="rpr-favorites-message" If={message}>Added to your favorites</span>
	<div class="rpr-favorites-add heart-beat" @click={addFavorite()}>
		<i class={isFavorite() ? 'rpr-icon icon-heart' : 'rpr-icon icon-heart-empty'} title="Add to favorites"></i><span If={count}>{count}</span>
	</div>
</script>
